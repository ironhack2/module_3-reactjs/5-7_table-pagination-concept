import { TableContainer, Table, TableBody, TableHead, TableRow, TableCell, styled, Pagination } from '@mui/material';

import { Container, Grid, Box, Modal, Button, TextField, Typography, FormControl, InputLabel, Select, MenuItem } from '@mui/material';


import { useState, useEffect } from 'react';



function DataTable() {

    //TABLE
    const [rows, setRow] = useState([]);


    //MODAL
    const [userInfo, setUserInfo] = useState({});
    const [open, setOpen] = useState(false);


    // PAGINATION
    const [limit, setLimit] = useState(10);
    const [page, setPage] = useState(1);
    const [noPage, setNoPage] = useState(0);

    const changePageHandler = (event, value) => {
        setPage(value);
    }

    //SelectNumberPage
    const changeSelectNumberPage = (value) => {
        setLimit(value.target.value);
    }

    //CALL API
    const fetchAPI = async (url) => {
        const response = await fetch(url);

        const data = await response.json();
        return data;
    }

    useEffect((data) => {
        fetchAPI("http://42.115.221.44:8080/devcamp-register-java-api/users")
            .then((data) => {
                setNoPage(Math.ceil(data.length / limit));

                setRow(data.slice((page - 1) * limit, page * limit));
            })
            .catch((error) => {
                console.error(error.message);
            })
    }, [page, limit])


    // TABLE
    const StyledTableRow = styled(TableRow)(({ theme }) => ({
        '&:nth-of-type(odd)': {
            backgroundColor: theme.palette.action.hover,
        },
        // hide last border
        '&:last-child td, &:last-child th': {
            border: 0,
        },
    }));



    //MODAL:

    const style = {
        position: 'absolute',
        top: '50%',
        left: '50%',
        transform: 'translate(-50%, -50%)',
        width: 400,
        bgcolor: 'background.paper',
        border: '2px solid #000',
        boxShadow: 24,
        p: 4,
    };

    const handleOpenModal = (value) => {
        console.log(value);
        setUserInfo(value);
        setOpen(true);
    };

    const handleCloseModal = () => {
        setOpen(false);
    };


    return (
        <Container>
            <Grid container  justifyContent={"center"} mt={5}>
                <Typography align="center" variant="h2"> Danh sách đăng ký</Typography>
            </Grid>

            <Grid container mt={5}>
                <Grid item>
                    <FormControl sx={{ m: 1, minWidth: 100 }}>
                        <InputLabel id="demo-simple-select-label">Select Number</InputLabel>
                        <Select
                            labelId="demo-simple-select-label"
                            id="demo-simple-select"
                            value={limit}
                            label="SelectNumberPage"
                            onChange={changeSelectNumberPage}>
                            <MenuItem value={5}>5</MenuItem>
                            <MenuItem value={10}>10</MenuItem>
                            <MenuItem value={25}>25</MenuItem>
                            <MenuItem value={50}>50</MenuItem>
                        </Select>
                    </FormControl>
                </Grid>
            </Grid>

            <Grid container>
                <Grid item xs={12}>
                    <TableContainer>
                        <Table sx={{ minWidth: 650 }} aria-label="simple table">
                            <TableHead>
                                <TableRow>
                                    <TableCell align="center"> <h3> Mã người dùng </h3> </TableCell>
                                    <TableCell align="center"> <h3> First Name</h3> </TableCell>
                                    <TableCell align="center"> <h3>Last Name </h3> </TableCell>
                                    <TableCell align="center"> <h3>Country </h3> </TableCell>
                                    <TableCell align="center"> <h3>Subject </h3> </TableCell>
                                    <TableCell align="center"> <h3>Customer Type </h3> </TableCell>
                                    <TableCell align="center"> <h3>Register Status </h3> </TableCell>
                                    <TableCell align="center"> <h3>Action </h3> </TableCell>
                                </TableRow>
                            </TableHead>

                            <TableBody>
                                {
                                    rows.map((row, index) => {
                                        return (
                                            <StyledTableRow key={index}>
                                                <TableCell align="center"> {row.id} </TableCell>
                                                <TableCell align="center"> {row.firstname} </TableCell>
                                                <TableCell align="center"> {row.lastname} </TableCell>
                                                <TableCell align="center"> {row.country} </TableCell>
                                                <TableCell align="center"> {row.subject} </TableCell>
                                                <TableCell align="center"> {row.customerType} </TableCell>
                                                <TableCell align="center"> {row.registerStatus} </TableCell>
                                                <TableCell align="center" style={{ width: "120px" }}>
                                                    <Button variant="contained" onClick={() => { handleOpenModal(row) }}> Chi tiết </Button>
                                                </TableCell>
                                            </StyledTableRow>
                                        )
                                    })
                                }
                            </TableBody>
                        </Table>
                    </TableContainer>
                </Grid>
            </Grid>

            <Grid container mt={3} mb={2} justifyContent="flex-end">
                <Grid item>
                    <Pagination count={noPage} defaultPage={page} color="primary" onChange={changePageHandler} />
                </Grid>
            </Grid>


            <Grid container>
                <Grid item>
                    <Modal open={open} onClose={handleCloseModal} aria-labelledby="parent-modal-title" aria-describedby="parent-modal-description">
                        <Box sx={{ ...style, width: 400 }}>
                            <Typography variant="h3" align="center">Thông Tin User</Typography>

                            <Grid container mt={5}>
                                <Grid item xs={4}>
                                    <label>ID: </label>
                                </Grid>
                                <Grid item xs={8}>
                                    <TextField default value={userInfo.id} />
                                </Grid>
                            </Grid>

                            <Grid container mt={1}>
                                <Grid item xs={4}>
                                    <label>First Name: </label>
                                </Grid>
                                <Grid item xs={8}>
                                    <TextField default value={userInfo.firstname} />
                                </Grid>
                            </Grid>

                            <Grid container mt={1}>
                                <Grid item xs={4}>
                                    <label>Last Name: </label>
                                </Grid>
                                <Grid item xs={8}>
                                    <TextField default value={userInfo.lastname} />
                                </Grid>
                            </Grid>

                            <Grid container mt={1}>
                                <Grid item xs={4}>
                                    <label>Country: </label>
                                </Grid>
                                <Grid item xs={8}>
                                    <TextField default value={userInfo.country} />
                                </Grid>
                            </Grid>

                            <Grid container mt={1}>
                                <Grid item xs={4}>
                                    <label>Subject: </label>
                                </Grid>
                                <Grid item xs={8}>
                                    <TextField default value={userInfo.subject} />
                                </Grid>
                            </Grid>

                            <Grid container mt={1}>
                                <Grid item xs={4}>
                                    <label>Customer Type: </label>
                                </Grid>
                                <Grid item xs={8}>
                                    <TextField default value={userInfo.customerType} />
                                </Grid>
                            </Grid>

                            <Grid container mt={1}>
                                <Grid item xs={4}>
                                    <label>Register Status: </label>
                                </Grid>
                                <Grid item xs={8}>
                                    <TextField default value={userInfo.registerStatus} />
                                </Grid>
                            </Grid>

                            <Grid container mt={5}  justifyContent={"end"}>
                                <Grid item>
                                    <Button variant="outlined" onClick={handleCloseModal}>Close Modal</Button>
                                </Grid>
                            </Grid>
                        </Box>
                    </Modal>
                </Grid>
            </Grid>

        </Container>
    )
}

export default DataTable;

