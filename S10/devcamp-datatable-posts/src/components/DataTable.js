import { Container, Grid, Button, Table, TableBody, TableContainer, TableHead, TableRow, TableCell, Paper, Pagination } from '@mui/material';
import { useEffect, useState } from 'react';



function DataTable() {

    const limit = 10;

    const [rows, setRow] = useState([])
    const [page, setPage] = useState(1);
    const [noPage, setNoPage] = useState(0);
    const changePageHandler = (event, value) => {
        setPage(value);
    }


    const fetchAPI = async (url) => {
        const response = await fetch(url);

        const data = await response.json();
        return data;
    }


    useEffect(() => {
        fetchAPI("https://jsonplaceholder.typicode.com/posts")
            .then((data) => {
                setNoPage(Math.ceil(data.length / limit));

                setRow(data.slice((page - 1) * limit, page * limit))
            })
            .catch((err) => {
                console.error(err.message);
            })
    }, [page])


    const detailBtnHandler = (value) => {
        console.log(value);
    }


    return (
        <div>
            <Container>
                <Grid container>
                    <Grid item>
                        <TableContainer>
                            <Table sx={{ minWidth: 650 }} aria-label="simple table">
                                <TableHead>
                                    <TableRow>
                                        <TableCell align="center">ID</TableCell>
                                        <TableCell align="center">User ID</TableCell>
                                        <TableCell align="center">Title</TableCell>
                                        <TableCell align="center">Body</TableCell>
                                        <TableCell align="center">Action</TableCell>
                                    </TableRow>
                                </TableHead>

                                <TableBody>
                                    {
                                        rows.map((row, index) => {
                                            return (
                                                <TableRow key={index}>
                                                    <TableCell align="center"> {row.id} </TableCell>
                                                    <TableCell align="center"> {row.userId} </TableCell>
                                                    <TableCell align="center"> {row.title} </TableCell>
                                                    <TableCell align="center"> {row.body} </TableCell>
                                                    <TableCell align="center"> <Button onClick={() => { detailBtnHandler(row) }}> Chi tiết </Button> </TableCell>
                                                </TableRow>
                                            )
                                        })
                                    }
                                </TableBody>
                            </Table>
                        </TableContainer>
                    </Grid>
                </Grid>

                <Grid container mt={3} mb={2} justifyContent="flex-end">
                    <Grid item>
                        <Pagination count={noPage} defaultPage={page} color="primary" onChange={changePageHandler} />
                    </Grid>
                </Grid>
            </Container>

        </div>
    )
}

export default DataTable;